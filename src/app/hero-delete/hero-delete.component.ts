import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';

import { Hero } from '../hero';
import { HeroService } from '../hero.service';
import { HEROES } from '../mock-heroes';

@Component({
  selector: 'app-hero-delete',
  templateUrl: './hero-delete.component.html',
  styleUrls: [ './hero-delete.component.css' ]
})
export class HeroDeleteComponent implements OnInit {
  hero: Hero = {} as Hero;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private heroService: HeroService,
    private location: Location
  ) {}

  ngOnInit() {
    const id = +this.route.snapshot.paramMap.get('id');
    if(!id) {
      return this.router.navigate(['/']);
    }

    this.heroService.getHero(id).subscribe(res => this.hero = res, err => {
      console.log(err);
      this.router.navigate(['/']);
    });
  }

  create() {
    this.heroService.create(this.hero.name,);
  }

  goBack(): void {
    this.location.back();
  }

  delete() {
    this.heroService.delete(this.hero).subscribe(() => {
      console.log("Success");
    }, error => {
      console.error(error);
    });
  }

}