import { Component, OnInit ,EventEmitter,Output} from '@angular/core';
import { ActivatedRoute, Router} from '@angular/router';
import { Location } from '@angular/common';

import { Hero } from '../hero';
import { HeroService } from '../hero.service';

@Component({
  selector: 'app-hero-detail',
  templateUrl: './hero-detail.component.html',
  styleUrls: [ './hero-detail.component.css' ]
})
export class HeroDetailComponent implements OnInit {
  hero: Hero = {} as Hero;
  
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private heroService: HeroService,
    private location: Location
  ) {}

  ngOnInit() {
    const id = +this.route.snapshot.paramMap.get('id');
    if(!id) {
      return this.router.navigate(['/']);
    }

    this.heroService.getHero(id).subscribe(res => this.hero = res, err => {
      console.log(err);
      this.router.navigate(['/']);
    });
  }

  getHero(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.heroService.getHero(id)
      .subscribe(hero => this.hero = hero);
  }

  goBack(): void {
    this.location.back();
  }

  delete(){
    this.heroService.delete(this.hero).subscribe(() => {
      console.log("Success");
      this.goBack();
    }, error => {
      console.error(error);
    });
  }

  update(){
    this.heroService.update(this.hero).subscribe(() => {
      console.log("Success");
    }, error => {
      console.error(error);
    });
  }
  
}