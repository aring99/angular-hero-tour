import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';

import { Hero } from '../hero';
import { HeroService } from '../hero.service';
import { HEROES } from '../mock-heroes';

@Component({
  selector: 'app-hero-update',
  templateUrl: './hero-update.component.html',
  styleUrls: [ './hero-update.component.css' ]
})
export class HeroUpdateComponent implements OnInit {
  hero: Hero = {} as Hero;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private heroService: HeroService,
    private location: Location
  ) {}

  ngOnInit() {
    const id = +this.route.snapshot.paramMap.get('id');
    if(!id) {
      return this.router.navigate(['/']);
    }

    this.heroService.getHero(id).subscribe(res => this.hero = res, err => {
      console.log(err);
      this.router.navigate(['/']);
    });
  }

  create() {
    this.heroService.create(this.hero.name,);
  }

  goBack(): void {
    this.location.back();
  }

  update() {
    this.heroService.update(this.hero).subscribe(() => {
      console.log("Success");
    }, error => {
      console.error(error);
    });
  }

}