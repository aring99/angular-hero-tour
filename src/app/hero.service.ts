import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';

import { Hero } from './hero';
// import { HEROES } from './mock-heroes';
import { MessageService } from './messages.service';

@Injectable({ providedIn: 'root' })
export class HeroService {
  HERO_KEY = 'HEROS';
  HEROS: any = [];
  heroCounter = 1;

  constructor(private messageService: MessageService) {
    this.HEROS = JSON.parse(localStorage.getItem(this.HERO_KEY)) || [];
  }

  getHeroes(): Observable<Hero[]> {
    // TODO: send the message _after_ fetching the heroes
    return of(this.HEROS);
  }

  getHero(id: number): Observable<Hero> {
    // TODO: send the message _after_ fetching the hero
    this.messageService.add(`Oluşturulan kahramanın numarası:${id}`);
    return of(this.HEROS.find(hero => hero.id === id));
  }

  create(name: string,power:number) {
    var hero = {
      id: this.heroCounter++,
      name: name,
      power: power
    }
    this.HEROS.push(hero);
    localStorage.setItem(this.HERO_KEY, JSON.stringify(this.HEROS));
    console.log("Hero created.");
  }

  update(hero: Hero): Observable<Hero> {
    return new Observable<Hero>((observer) => {
      if(!hero) {
        return observer.error(new Error("Hero not found."));
      }
      localStorage.setItem(this.HERO_KEY, JSON.stringify(this.HEROS));
      observer.next(hero);
    });
  }

  delete(hero: Hero): Observable<Hero> {
    return new Observable<Hero>((observer) => {
      if(!hero) {
        return observer.error(new Error("Hero not found."));
      }
      debugger;
      this.HEROS = this.HEROS.filter(h => h.id !== hero.id)
      localStorage.setItem(this.HERO_KEY, JSON.stringify(this.HEROS));
      observer.next(hero);
    });
  }
  delete(hero: Hero): Observable<Hero> {
    return new Observable<Hero>((observer) => {
      if(!hero) {
        return observer.error(new Error("Hero not found."));
      }
      this.HEROS= this.HEROS.filter(h => h.id !== hero.id)
      localStorage.setItem(this.HERO_KEY, JSON.stringify(this.HEROS));
      observer.next(hero);
    });
  }
}