import { Hero } from './hero';

export const HEROES: Hero[] = [
  { id: 11, name: 'Dr Nice' , power: 10},
  { id: 12, name: 'Narco' , power: 15},
  { id: 13, name: 'Bombasto' , power: 20},
  { id: 14, name: 'Celeritas' , power: 18},
  { id: 15, name: 'Magneta' , power: 14},
  { id: 16, name: 'RubberMan' , power: 12},
  { id: 17, name: 'Dynama' , power: 11},
  { id: 18, name: 'Dr IQ' , power: 19},
  { id: 19, name: 'Magma' , power: 13},
  { id: 20, name: 'Tornado' , power: 17}
];